import socket
import struct
from time import sleep

MCAST_GROUP = '224.1.1.1'
MCAST_PORT = 42042
MCAST_SOCK = (MCAST_GROUP, MCAST_PORT)
MCAST_RECV_SIZE = 4096
MCAST_TTL = 1

def configure_socket() -> socket.SocketType:
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setsockopt(socket.IPPROTO_IP,
                    socket.IP_ADD_MEMBERSHIP,
                    struct.pack('4sl', socket.inet_aton(MCAST_GROUP), socket.INADDR_ANY))
    return sock


def recv(repeat: bool = False) -> bytes:
    sock = configure_socket()
    sock.bind(MCAST_SOCK)
    if repeat:
        while True:
            print(sock.recv(MCAST_RECV_SIZE))
    else:
        return sock.recv(MCAST_RECV_SIZE)


def send(msg: str | bytes, repeat: bool = False):
    sock = configure_socket()
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, MCAST_TTL)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_LOOP, 0)

    if isinstance(msg, str):
        msg = msg.encode('utf-8')

    if repeat:
        while True:
            sock.sendto(msg, MCAST_SOCK)
            sleep(1)
    else:
        sock.sendto(msg, MCAST_SOCK)


# Tests
if __name__ == '__main__':
    choice = int(input('1: Receive\n2: Send\n> '))
    if choice == 1:
        recv()
    elif choice == 2:
        send(input('Message: '), repeat=True)
    else:
        print('Invalid choice. Bye-bye!')
        exit(0)
