import socket
import time
import multiprocessing as mp
import random
import logging

from dataclasses import dataclass
from collections import namedtuple
from rich.logging import RichHandler

from . import multicast, event, message

logging.basicConfig(
    level='NOTSET', format='%(message)s', datefmt='[%X]', handlers=[RichHandler()]
)
log = logging.getLogger()


Delay = namedtuple('Delay', 'min max')


class DeadProcessError(Exception):
    pass


@dataclass
class Process:
    id: str
    hostname: str
    chance: float
    events: int
    delay: Delay
    socket: socket.SocketType


def wait_for_mcast_signal():
    while True:
        try:
            msg = multicast.recv()
            msg = msg.decode('utf-8')
            log.debug(f'Received multicast data: {msg}')
            if msg == 'START':
                return
        except Exception as e:
            log.exception(e)
            continue


def create_socket(port: int) -> socket.SocketType:
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.bind(('', port))
    return sock


def update_clocks(our_id: int, nclocks: dict[str, int], recvd: dict[str, int]):
    for _id, clk in recvd.items():
        if _id == our_id:
            continue
        nclocks[_id] = max(clk, nclocks[_id])


def get_random_neighbor() -> dict:
    global neighbors
    return random.choice(list(neighbors.values()))


def vector_clock_to_lstr(clk: dict[str, int]) -> list[str]:
    return list(map(lambda t: str(t[1]), sorted(list(clk.items()), key=lambda t: t[0])))


def vector_clock_to_str(clk: dict[str, int]) -> str:
    vclk = vector_clock_to_lstr(clk)
    return '[' + ','.join(vclk) + ']'


def log_event(*args: str):
    print(' '.join(args))


def do_remote_event():
    global process, neighbor_clocks
    neighbor = get_random_neighbor()
    n = (neighbor['host'], neighbor['port'])
    msg = message.Message(_from=process.id, clocks=dict(neighbor_clocks)) #type: ignore
    log.info(f'Sending {msg.clocks} to {n}')
    log_event(process.id, vector_clock_to_str(dict(neighbor_clocks)), 'S', neighbor['id'])
    try:
        process.socket.sendto(msg.serialize(), n)
    except socket.gaierror:
        msg = f'Tried to send data to dead process: {n}'
        raise DeadProcessError(msg)


def do_event():
    global process, neighbor_clocks
    event_type = event.Event.LOCAL
    chance = random.uniform(0, 1)
    if chance <= process.chance:
        # Send message
        event_type = event.Event.REMOTE
    log.info(event_type)
    neighbor_clocks[process.id] = neighbor_clocks[process.id] + 1
    if event_type == event.Event.REMOTE:
        do_remote_event()
    else:
        log_event(process.id, vector_clock_to_str(dict(neighbor_clocks)), 'L')


def socket_listener(sock: socket.SocketType, nclocks: dict, _id: int):
    while True:
        data = sock.recv(4096)
        msg = message.Message.deserialize(data)
        update_clocks(_id, nclocks, msg.clocks)
        nclocks[_id] = nclocks[_id] + 1
        log.info(f'Received from {msg._from}')
        log.debug(nclocks)
        log_event(process.id, vector_clock_to_str(dict(neighbor_clocks)), 'R', msg._from, vector_clock_to_str(msg.clocks))


def start_socket_listener(proc: mp.Process):
    proc.start()


def main(should_log: bool, config: dict, neighbors_info: dict[str, dict]):
    if not should_log:
        log.setLevel(logging.CRITICAL + 1)

    global process, neighbors, neighbor_clocks

    m = mp.Manager()
    neighbor_clocks = m.dict({neighbor: 0 for neighbor in neighbors_info.keys()})
    process = Process(id=config['id'],
                      hostname=config['host'],
                      chance=config['chance'],
                      events=config['events'],
                      delay=Delay(config['min_delay'], config['max_delay']),
                      socket=create_socket(config['port']))
    neighbors = neighbors_info
    neighbor_clocks[process.id] = 0

    worker = mp.Process(target=socket_listener, args=(process.socket, neighbor_clocks, process.id, ))

    log.info('Start')
    start_socket_listener(worker)
    log.info('Socket listener ready')
    log.info('Waiting for multicast signal')
    wait_for_mcast_signal()
    log.info('Multicast signal received. Starting.')

    for _ in range(process.events):
        try:
            do_event()
        except DeadProcessError as e:
            log.fatal(e)
            break
        time.sleep(random.randint(process.delay.min, process.delay.max) / 1000)

    worker.kill()
    final_info = f'{process.id}|{process.hostname}|{neighbor_clocks[process.id]}|{sorted(list(neighbor_clocks.items()), key=lambda t: t[0])}'
    log.info(final_info)
    multicast.send(final_info)
