from enum import Enum, auto

class Event(Enum):
    LOCAL = auto()
    REMOTE = auto()
