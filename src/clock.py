from dataclasses import dataclass

@dataclass
class VectorClock:
    _clk: int

    def signal(self) -> None:
        self._clk += 1

    @property
    def clock(self) -> int:
        return self._clk
