from dataclasses import dataclass
import pickle

MESSAGE_SEPARATOR = '*~~*.*~~*'

@dataclass
class Message:
    _from: str
    clocks: dict[str, int]

    @staticmethod
    def deserialize(data: bytes) -> 'Message':
        return pickle.loads(data)

    def serialize(self) -> bytes:
        return pickle.dumps(self)
