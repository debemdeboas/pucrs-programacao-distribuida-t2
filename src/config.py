
import json

CONFIGURATION_FILE_PATH = 'config/.conf'
CONFIGURATION_LOG_FILE_PATH = 'config/log.json'
KEYS = 'id host port chance events min_delay max_delay'.split(' ')

def read_configuration_file(line_no: int = -1) -> tuple[dict, dict[str, dict]]:
    with open(CONFIGURATION_FILE_PATH) as f:
        lines = list(map(lambda l: l.split(' '), f.readlines()))

    config = {}
    if line_no >= 0:
        our_line = lines[line_no]
        lines.pop(line_no)
        config: dict[str, str | int | float] = dict(zip(KEYS, our_line))
        config['port'] = int(config['port'])
        config['chance'] = float(config['chance'])
        config['events'] = int(config['events'])
        config['min_delay'] = int(config['min_delay'])
        config['max_delay'] = int(config['max_delay'])

    neighbors: dict[str, dict] = {}
    for line in lines:
        their_config: dict[str, str | int | float] = dict(zip(KEYS, line))
        their_config['port'] = int(their_config['port'])
        their_config['chance'] = float(their_config['chance'])
        their_config['events'] = int(their_config['events'])
        their_config['min_delay'] = int(their_config['min_delay'])
        their_config['max_delay'] = int(their_config['max_delay'])
        neighbors[str(their_config['id'])] = their_config
    return config, neighbors


def read_log_config() -> bool:
    with open(CONFIGURATION_LOG_FILE_PATH) as f:
        obj = json.load(f)
    return bool(obj.get('log_to_stdout', True))
