import sys
from src.config import read_configuration_file, read_log_config
from src.process import main

if __name__ == '__main__':
    line_no = int(sys.argv[1])
    main(read_log_config(), *read_configuration_file(line_no))
