terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.23.0"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

variable "process_count" {
  type        = number
  description = "Number of processes"
  default     = 4
  validation {
    condition     = var.process_count >= 3
    error_message = "Process count should be greater than 3!"
  }
}

variable "events" {
  type        = number
  description = "Number of events"
  default     = 100
  validation {
    condition     = var.events > 0
    error_message = "Number of events must be greater than 0!"
  }
}

resource "random_integer" "event_chance" {
  count = var.process_count
  min   = 0
  max   = 100
}

resource "random_integer" "min_delay" {
  count = var.process_count
  min   = 100
  max   = 300
}

resource "random_integer" "max_delay" {
  count = var.process_count
  min   = 350
  max   = 750
}

resource "random_pet" "hostname" {
  count  = var.process_count
  length = 1
}

resource "docker_image" "process" {
  name         = "dist-prog-process"
  force_remove = false

  build {
    path       = "."
    dockerfile = "docker/process/Dockerfile"
    tag        = ["dist-prog-process:latest"]
  }

  triggers = {
    dir_sha1 = sha1(join("", concat(
      [for f in fileset(path.module, "src/**/*.py") : filesha1(f)],
      [for f in fileset(path.module, "src/*.py") : filesha1(f)],
      [for f in fileset(path.module, "docker/**/*") : filesha1(f)],
      [filesha1("requirements.txt"), filesha1("process.py"), filesha1("main.py")]
    )))
  }
}

resource "docker_image" "manager" {
  name         = "dist-prog-manager"
  force_remove = false

  build {
    path       = "."
    dockerfile = "docker/manager/Dockerfile"
    tag        = ["dist-prog-manager:latest"]
  }

  triggers = {
    dir_sha1 = sha1(join("", concat(
      [for f in fileset(path.module, "src/**/*.py") : filesha1(f)],
      [for f in fileset(path.module, "src/*.py") : filesha1(f)],
      [for f in fileset(path.module, "docker/**/*") : filesha1(f)],
      [filesha1("requirements.txt"), filesha1("process.py"), filesha1("main.py")]
    )))
  }
}

resource "docker_network" "dist_prog_t2_network" {
  name       = "dist-prog-t2-network"
  driver     = "bridge"
  attachable = true
  ipv6       = false
}

resource "local_file" "process_ids" {
  content = join("\n", [
    for i in range(var.process_count) :
    join(" ", [
      i,
      join("-", ["proc", i, random_pet.hostname[i].id]),
      42040,
      random_integer.event_chance[i].result / 100,
      var.events,
      random_integer.min_delay[i].result,
      random_integer.max_delay[i].result
    ])
  ])

  filename = "${path.module}/config/.conf"
}

resource "docker_container" "processes" {
  count = var.process_count
  depends_on = [
    docker_image.process,
    docker_network.dist_prog_t2_network,
    local_file.process_ids
  ]

  name       = join("-", ["proc", count.index, random_pet.hostname[count.index].id])
  image      = "dist-prog-process:latest"
  tty        = true
  stdin_open = true
  start      = true
  rm         = false

  networks_advanced {
    name = "dist-prog-t2-network"
  }

  env = ["LINE_NO=${count.index}"]

  volumes {
    container_path = "/app/config"
    host_path      = abspath("${path.module}/config")
    read_only      = true
  }
}

resource "docker_container" "manager" {
  depends_on = [
    docker_image.manager,
    docker_network.dist_prog_t2_network,
    local_file.process_ids,
    docker_container.processes
  ]

  name       = "manager"
  image      = "dist-prog-manager:latest"
  tty        = true
  stdin_open = true
  start      = true
  rm         = false

  networks_advanced {
    name = "dist-prog-t2-network"
  }

  volumes {
    container_path = "/app/config"
    host_path      = abspath("${path.module}/config")
    read_only      = true
  }
}
