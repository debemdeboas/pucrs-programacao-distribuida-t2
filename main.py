import logging

from rich.logging import RichHandler
from rich.table import Table
from rich.console import Console

from src import multicast, config

logging.basicConfig(
    level='NOTSET', format='%(message)s', datefmt='[%X]', handlers=[RichHandler()]
)
log = logging.getLogger()

_, conf = config.read_configuration_file()
input()

recvd_logs_amount = 0
processes_amount = len(conf)

table = Table(title='Process logs', highlight=False, title_justify='full', show_lines=True)
table.add_column('ID', justify='center', style='cyan')
table.add_column('Name', justify='left', style='magenta')
table.add_column('Local clock', justify='center', style='green')
table.add_column('Vector clocks', justify='left', style='green')

log.info('Sending START message to all processes')
multicast.send('START', repeat=False)

rows = []
while recvd_logs_amount < processes_amount:
    log.info('Waiting for logs')
    logs = multicast.recv()
    l = list(map(lambda s: s.strip(), logs.decode('utf-8').split('|')))
    l[-1] = ', '.join(f'{k}: {v}' for k, v in dict(eval(l[-1])).items())
    rows.append(l)
    recvd_logs_amount += 1

[table.add_row(*l) for l in sorted(rows)]
console = Console()
console.print(table)
