# [PUCRS - Programação Distribuída - T2](https://gitlab.com/debemdeboas/pucrs-programacao-distribuida-t2)

Este trabalho consiste em desenvolver um sistema distribuído que implementa
relógios vetoriais para ordenação total de eventos.
Autorado por Rafael Almeida de Bem e Vicente Vivian.

## Arquivo de configuração

O formato de cada linha do arquivo de configuração do sistema é:

```
id host port chance events min_delay max_delay
```

Onde:

- `id`: número inteiro que identifica um processo;
- `host`: hostname ou endereço IP do nodo que executa o processo;
- `port`: número da porta que o processo vai escutar;
- `chance`: probabilidade (entre $0$ e $1$) da ocorrência de um evento de envio de mensagem. Por exemplo, $0.2$ significa $20\%$ de probabilidade de ser realizado um envio, sendo os $80\%$ restantes eventos locais;
- `events`: número de eventos que serão executados nesse nodo;
- `min_delay`: tempo mínimo de intervalo entre eventos (em milissegundos);
- `max_delay`: tempo máximo de intervalo entre eventos (em milissegundos).

Valores sugeridos:

- `events`: $100$
- `min_delay`: entre $100$ e $300$
- `max_delay`: entre $350$ e $750$

## Inicialização

Inicialmente os processos se conectam ao grupo multicast `224.1.1.1` na porta `42042` e esperam
um pacote de início para poderem todos iniciarem seus sistemas de maneira "síncrona".

## Execução

Após rodar o script `run.sh` devemos rodar o comando `docker attach manager` para
podermos iniciar o sistema. Ao entrarmos no `manager` devemos apertar <kbd>RETURN</kbd>. Isso
enviará uma mensagem via multicast para os processos indicando que podem começar suas execuções.
Um exemplo de execução segue:

```shell
$ docker attach manager

[17:49:18] INFO     Sending START message to all processes          main.py:26
           INFO     Waiting for logs                                main.py:31
[17:49:21] INFO     Waiting for logs                                main.py:31
[17:49:22] INFO     Waiting for logs                                main.py:31
[17:49:23] INFO     Waiting for logs                                main.py:31
Process logs
┏━━━━┳━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ ID ┃ Name            ┃ Local clock ┃ Vector clocks             ┃
┡━━━━╇━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━┩
│ 0  │ proc-0-flea     │     14      │ 0: 14, 1: 3, 2: 0, 3: 3   │
├────┼─────────────────┼─────────────┼───────────────────────────┤
│ 1  │ proc-1-seagull  │     14      │ 0: 9, 1: 14, 2: 10, 3: 10 │
├────┼─────────────────┼─────────────┼───────────────────────────┤
│ 2  │ proc-2-doberman │     14      │ 0: 9, 1: 3, 2: 14, 3: 11  │
├────┼─────────────────┼─────────────┼───────────────────────────┤
│ 3  │ proc-3-heron    │     12      │ 0: 9, 1: 14, 2: 10, 3: 12 │
└────┴─────────────────┴─────────────┴───────────────────────────┘
```
